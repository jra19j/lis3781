# LIS3781 - Advanced Database Management

## Jose Rafael Asmar

### Assignment 4 Requirements:

*Three parts:*

1. Create and populate tables using SQL Server
2. Complete reports using populated tables
3. Answer chapter questions

#### Assignment Screenshots:

#### Screenshot of ERD:

![SS_Tables1](img/A4ERD.PNG "A4 ERD Screenshot")

#### Tutorial Links:

*Bitbucket tutorial - Station locations*
[Bitbucket tutorial link](https://bitbucket.org/jra19j/bitbucketstationlocation/src/master/ "Bitbucket Station Locations)