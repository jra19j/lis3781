# LIS3781 - Advanced Database Management

## Jose Rafael Asmar

### Assignment 2 Requirements:

*Three parts:*

1. Create and populate tables using MySQL
2. Create new users 
3. Demonstrate SQL commands using new users

#### Assignment Screenshots:

#### Screenshot of populated tables:

![SS_Tables](img/A2Tables.png "Populated database customer + company")

#### Screenshot of grants for root user:

![SS_Grants2](img/A2Grants2.png "Grants for root user")

#### Screenshot of grants for user 1:

![SS_Grants3](img/A2Grants3.png "Screenshot of grants for user 1")

#### Screenshot of grants for user 2:

![SS_Grants1](img/A2Grants1.png "Screenshot of grants for user 2")

#### Screenshot of logged in user 2 with MySQL version:

![SS_U2Proof](img/A2User2Proof.png "Screenshot of logged in user 2 with MySQL version")

#### Screenshot of table structure:

![SS_AProof](img/A2AdminStructure.png "Screenshot of table structure")

#### Screenshot of attempt to display data from company table as user 2:

![SS_U2Data](img/A2User2Company.png "Screenshot of attempt to display data from company table as user 2")

#### Screenshot of attempt to display data from customer table as user 1:

![SS_U1Data](img/A2User1Customer.png "Screenshot of attempt to display data from customer table as user 1")

#### Screenshot of attempt to insert data into both tables as user 1:

![SS_U1Insert](img/A2User1Inserts.png "Screenshot of attempt to insert data into both tables as user 1")

#### Screenshot of attempt to delete data from customer as user 2:

![SS_U2Delete](img/A2User2Delete.png "Screenshot of attempt to delete data from customer as user 2")

#### Screenshot of removal of tables as root user:

![SS_ADelete](img/A2AdminDelete.png "Screenshot of removal of tables as root user")

#### Tutorial Links:

*Bitbucket tutorial - Station locations*
[A1 Bitbucket tutorial link](https://bitbucket.org/jra19j/bitbucketstationlocation/src/master/ "Bitbucket Station Locations)