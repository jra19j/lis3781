/*
A character set is a set of symbols and encodings. 

A collation is a set of rules for comparing characters in a character set.

Suppose that we have an alphabet with four letters: "A", "B", "a", "b". 

We give each letter a number: "A" = 0, "B" = 1, "a" = 2, "b" = 3. 

The letter "A" is a symbol, the number 0 is the encoding for "A", and the 
combination of all four letters and their encodings is a character set. 

http://dev.mysql.com/doc/refman/5.5/en/charset.html

http://www.go4expert.com/forums/showthread.php?t=21166
*/

drop database if exists jra19j;
create database if not exists jra19j;
use jra19j;

------------------------
-- Table company
------------------------
DROP TABLE IF EXISTS company;
CREATE TABLE IF NOT EXISTS company
()