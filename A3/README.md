# LIS3781 - Advanced Database Management

## Jose Rafael Asmar

### Assignment 3 Requirements:

*Three parts:*

1. Create and populate tables using Oracle SQL Developer
2. Complete reports using populated tables
3. Answer chapter questions

#### Assignment Screenshots:

#### Screenshot of populated tables:

![SS_Tables1](img/A3Tables1.png "Populated database image 1")
![SS_Tables2](img/A3Tables2.png "Populated database image 2")
![SS_Tables3](img/A3Tables3.png "Populated database image 3")

#### Screenshot of SQL code used to create the tables:

![SS_Code1](img/A3Code1.png "SQL Code screenshot 1")
![SS_Code2](img/A3Code2.png "SQL Code screenshot 2")
![SS_Code3](img/A3Code3.png "SQL Code screenshot 3")

#### Tutorial Links:

*Bitbucket tutorial - Station locations*
[A1 Bitbucket tutorial link](https://bitbucket.org/jra19j/bitbucketstationlocation/src/master/ "Bitbucket Station Locations)