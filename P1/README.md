# LIS3781 - Advanced Database Management

## Jose Rafael Asmar

### Project 1 Requirements:

*Three parts:*

1. Create and populate tables using SQL Script
2. Complete reports using populated tables
3. Answer chapter questions

#### Assignment Screenshots:

#### Screenshot of ERD:

![P1_ERD](img/P1ERD.png "ERD Image")

#### Screenshot of Person table:

![P_Table](img/Persontable.png "Person table screenshot")

#### Tutorial Links:

*Bitbucket tutorial - Station locations*
[A1 Bitbucket tutorial link](https://bitbucket.org/jra19j/bitbucketstationlocation/src/master/ "Bitbucket Station Locations)