# LIS3781 - Advanced Database Management

## Jose Rafael Asmar

### Assignment 1 Requirements:

*Five parts:*

1. Distributed version control with git and Bitbucket
2. AMPPS Installation
3. Questions
4. Entity relaionship diagram, and SQL code
5. Bitbucket repo links:
 
a) this assignment and

b) the completed tutorial (bitbucketstationlocations)

#### git commands with short descriptions:
    
    - git init used to start a new repository
    - git status used to list files pending commit
    - git add used to add a file to the staging area
    - git commit used to commit files added using git add
    - git push used to push committed files to repository
    - git pull used to fetch repo changes to current working directory
    - git stash used to temporarily store staged files

### Business Rules 
>
>The human resource (HR) department of the ACME company wants to contract a database 
>modeler/designer to collect the following employee data for tax purposes: job description, length of 
>employment, benefits, number of dependents and their relationships, DOB of both the employee and any 
>respective dependents. In addition, employees’ histories must be tracked. Also, include the following 
>business rules: 
>
>• Each employee may have one or more dependents. 

>• Each employee has only one job.  

>• Each job can be held by many employees. 

>• Many employees may receive many benefits. 

>• Many benefits may be selected by many employees (though, while they may not select any benefits— any dependents of employees may be on an employee’s plan). 
>
>Notes:  

>• Employee/Dependent tables must use suitable attributes (See Assignment Guidelines); 
>
>In Addition: 

>• Employee: SSN, DOB, start/end dates, salary; 

>• Dependent: same information as their associated employee (though, not start/end dates), date added (as dependent), type of relationship: e.g., father, mother, etc. 

>• Job: title (e.g., secretary, service tech., manager, cashier, janitor, IT, etc.) 

>• Benefit: name (e.g., medical, dental, long-term disability, 401k, term life insurance, etc.) 

>• Plan: type (single, spouse, family), cost, election date (plans must be unique) 

>• Employee history: jobs, salaries, and benefit changes, as well as who made the change and why; 

>• Zero Filled data: SSN, zip codes (not phone numbers: US area codes not below 201, NJ); 

>• *All* tables must include notes attribute. 
>
>Design Considerations: Generally, avoid using flag values (e.g., yes/no) for status—unless, applicable. 
>Instead, use dates when appropriate, as date values provide more information, and sometimes, can be 
>used when a flag value would be used. For example, “null” values in employees’ termination dates would 
>indicate “active” employees. 
>In addition, for federal, state, and local mandates, most HR systems require extensive history-keeping. 
>That is, virtually every change made to an employee record needs to be logged in a history table(s)—here, 
>we are keeping the design simple. Also, for reporting (and design) purposes, all *current* data should be 
>kept in the primary table (employee). Every time an employee’s data changes, it should be logged in the 
>history table, including when, why, and who made the change—crucial for reporting purposes!
>

#### Assignment Screenshots:

#### MySQL Installation:

![MYSQL_Inst](img/SQLRuning.png "My Installation")

#### A1 ERD:

![A1_ERD_PNG](img/A1_erd.PNG "My A1 ERD")

#### A1 Example 1:

![A1_EX1_PNG](img/A1EX1.png "My A1 example 1 PNG")

#### A1 Example 2:

![A1_EX2_PNG](img/A1EX2.png "My A1 example 2 PNG")

#### A1 Example 3:

![A1_EX3_PNG](img/A1EX3.png "My A1 example 3 PNG")

#### A1 Example 4:

![A1_EX4_PNG](img/A1EX4.png "My A1 example 4 PNG")

#### A1 Example 5:

![A1_EX5_PNG](img/A1EX5.png "My A1 example 5 PNG")

#### A1 Example 6:

![A1_EX6_PNG](img/A1EX6.png "My A1 example 6 PNG")

#### A1 Example 7:

![A1_EX7_PNG](img/A1EX7.png "My A1 example 7 PNG")

#### Tutorial Links:

*Bitbucket tutorial - Station locations*
[A1 Bitbucket tutorial link](https://bitbucket.org/jra19j/bitbucketstationlocation/src/master/ "Bitbucket Station Locations)