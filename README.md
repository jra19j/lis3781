# LIS3781 - Advanced Database Management

## Jose Rafael Asmar

### LIS3781 Requirements:

*Course work links:*

1. [A1 README.md](A1/README.md "My A1 README file")
    - Install AMPPS
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2 README.md](A2/README.md "My A2 README file")
    - Create and populate tables using MySQL
    - Create new users
    - Demonstrate SQL commands using new users

3. [A3 README.md](A3/README.md "My A3 README file")
    - Create and populate tables using Oracle SQL Developer
    - Complete reports using populated tables
    - Answer chapter questions

4. [A4 README.md](A4/README.md "My A4 README file")
    - Create and populate tables using SQL Server
    - Complete reports using populated tables
    - Answer chapter questions

5. [A5 README.md](A5/README.md "My A5 README file")
    - Create and populate tables using SQL Server
    - Complete reports using populated tables
    - Answer chapter questions

6. [P1 README.md](P1/README.md "My P1 README file")
    - Create and populate tables using SQL Script
    - Complete reports using populated tables
    - Answer chapter questions

7. [P2 README.md](P2/README.md "My P2 README file")
    - Install and set up MongoDB
    - Answer chapter questions
    - Preform and screenshot required reports