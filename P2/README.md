# LIS3781 - Advanced Database Management

## Jose Rafael Asmar

### Project 2 Requirements:

*Three parts:*

1. Intsall and set up MongoDB
2. Complete reports and submit screenshots
3. Answer chapter questions

#### Assignment Screenshots:

#### Screenshot of MongoDB Shell:

![P2_Shell](img/P2Shell.png "Shell Image")

#### Screenshot of Restaurant query:

![P_Table1](img/RestQuery1.png "query screenshot")

#### Tutorial Links:

*Bitbucket tutorial - Station locations*
[A1 Bitbucket tutorial link](https://bitbucket.org/jra19j/bitbucketstationlocation/src/master/ "Bitbucket Station Locations)